README for Lisp Tic-Tac-Toe

The files in this project consist of this README and ttt.lisp which runs the tic tac toe game. Instructions on how to play the game are down below,

This is a recreation of the classic tic-tac-toe game written in Common Lisp.
To run this game make sure Common Lisp is downloaded on your device. Then type in " (load "<pathname of ttt.lisp>") ".  Once it's loaded you should see #P"/Users/alimohamud/Desktop/LispTacToe/ttt.lisp". 

To play the multiplayer version type "(run-game)" without the quotation marks. The game will then ask for the first player to pick a spot, X always goes first. 
The spots are ordered like below:
0   1    2
3   4    5
6   7    8

To play against the AI as X type "(run-x)", then pick your spots and play.

To play against the AI as O type "(run-o)"  then pick your spots and play.

To watch the AI play against itself type "(ai-v-ai)". 

Once finished you'll see "Game Complete! " along with the final tac tac toe board.

To play again type " (load "<pathname of ttt.lisp>") " once more and play, Enjoy!
