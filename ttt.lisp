;; ttt.lisp
;;
;; Functions to help play a game of tic-tac-toe
;; Ali Mohamud
;;
;; list indices are:
;; 0 1 2
;; 3 4 5
;; 6 7 8


;; Prints a tic-tac-toe board in a pretty fashion.
;; Param: board - a list containing elements of a ttt board in row-major order
(defun print-board (board)
  (format t "=============")
  (do ((i 0 (+ i 1)) ) ;; i 0 is assignment + i 1 is iteration
      ((= i 9) nil);; if i = 9 break out of loop
      (if (= (mod i 3) 0)
          (format t "~%|")
          nil)
      (format t " ~A |" (nth i board))
  )
  (format t "~%=============")
)

;; Tests whether all three items in a list are equal to each other
;; and ignores dashes since they represent empty spaces.
;; Param: list - a list with three elements
(defun threequal (list)
  (if (equal (first list) '-)
    nil
    (and (equal (first list) (second list))
       (equal (second list) (third list))))
)

;; Grabs the nth row of a tic-tac-toe board as a list
;; Rows begin at 0 so rows are 0, 1 , 2
;; Param: board - a list representing a tic tac toe board
;; Param: row - an integer indicating the index of each row start
(defun grab-row (board row)
  (let ( (x (* 3 row)))
    (list (nth x board) (nth (+ 1 x) board) (nth (+ 2 x) board))
  )
)

;; Grabs the nth column of a tic-tac-toe board as a list
;; Columns are 0, 3, 6
;; Param: board - a list representing a tic tac toe board
;; Param: col - an integer indicating the index of each column start
(defun grab-col (board col)
  (list (nth col board) (nth (+ 3 col) board) (nth (+ 6 col) board)))

;;Grabs the nth item of the board in a slash pattern
;;The indices for a slash are 0, 4, 8
;; Param: board - a list representing a tic tac toe board
(defun grab-slash (board)
 (list (nth 0 board) (nth 4 board) (nth 8 board)))

 ;;Grabs the nth item of the board in a backslash pattern
 ;;The indices for a backslash are 2, 4, 6
 ;;Param: board - a list representing a tic tac toe board
 (defun grab-backslash (board)
  (list (nth 2 board) (nth 4 board) (nth 6 board)))

;;Determines whether a row has been won by checking if all
;;three values of each row are equal using threequal
;;Param: board - a list representing a tic tac toe board
(defun row-wonp (board)
  (or (threequal (grab-row board 0))
      (threequal (grab-row board 1))
      (threequal (grab-row board 2)))
      )

;;Determines whether a column has been won by checking if all
;;three values of each column are equal using threequal
;;Param: board - a list representing a tic tac toe board
(defun col-wonp (board)
  (or (threequal (grab-col board 0))
      (threequal (grab-col board 1))
      (threequal (grab-col board 2)))
      )

;;Determines whether a slash has been won by checking if all
;;three values of the slash or backslash patter are equal using threequal
;;Param: board - a list representing a tic tac toe board
(defun slash-wonp (board)
  (or (threequal (grab-slash board))
      (threequal (grab-backslash board)))
  )

;;Determines if the game is won by checking calling other Functions
;;to see if there's 3 in a row in a column, row or slash
;;Param: board - a list representing a tic tac toe board
(defun game-wonp (board)
  (or (row-wonp board)
      (col-wonp board)
      (slash-wonp board)))

;;This is a helper function for place-move
;;It works by figuring out the number of dashes in a list
;;It does this recursively and returns the count at the end
;;Param: Board - A list representing the tic tac toe board
(defun dash-count (board)
    (if (null board)
        0
        (if (eql(first board) '-)
            (+ 1 (dash-count(rest board)))
            (dash-count(rest board)))))

;;This function places a move for the player
;;It first checks if the spot is empty and
;;then places either an x or o depending on whose turn it is
;;Param: board  - A list representing the tic tac toe Board
;;Param; move - An integer representing the index in boars
(defun place-move (board move)
    (if (not(legal-movep board move))
      (format t "Spot occupied, try again. ~%")
      (if ( = (mod (dash-count board) 2) 0)
          (setf (nth move board) 'o)
          (setf (nth move board) 'x)))
    )



;;Checks if move space is currently empty
;;move is number 0 to 8 representing nth element of board list
(defun legal-movep (board move)
 (eql (nth move board) '-)
  )

;;Tests the legal-movep function returning t if all tests pass
(defun test-legal-movep ()

  (and (eql t (legal-movep '(- - - - - - - - -) 5))
        (null (legal-movep '(- - - - - - - - x) 8)))
)

;;Tests the place-move function returning t if all tests pass
(defun test-place-move ()
 (and(eql 'x (place-move '(- - - - - - - - -) 5))
      (eql 'o (place-move '(- - - x - - - - -) 5))))




;;Tests the game-wonp function returning t if all tests pass
(defun test-game-wonp ()
  (and(eql t (game-wonp '(x x x - - - - - -)))
      (eql t (game-wonp '(x - - x - - x - -)))
      (eql t (game-wonp '(x - - - x - - - x)))
      (eql nil(game-wonp '(- - - - - - - - -)))))

;;Runs all the other testing functions returning t if every
;;function passes all tests
(defun test-allp ()
  (format t "testing all user functions...")
  (and (test-legal-movep)
       (test-place-move)
       (test-game-wonp)))

;;This is a helper for run-game.
;;It prompts the user to pick a spot then reads the
;;spot into place-move
;;Param: board  - A list representing the tic tac toe board
(defun get-move (board)
      (princ "Pick a spot on the board: ")
      (place-move board (read)))

;;This is the driver of the multiplayer game.
;;It runs recursively checking if the Game
;;is finished each time and if so stops the recursion.
(defun run-game ()
  (let ((board '(- - - - - - - - -)))
    (if (or (game-wonp board) (= (dash-count board) 0))
        (progn
        (format t "Game Complete! ~%")
        (print-board board))

        (progn
        (get-move board)
        (print-board board)
        (format t "~%")
        (run-game)))))


;;This runs an AI that plays tic tac toe by picking a
;;random spot
;;Param: board  - A list representing the tic tac toe board
(defun random-ai (board)
  (let ((spot (random 9)))
      (if (eql '- (nth spot board))
          (place-move board spot)
          (random-ai board))))




;;This is the driver to watch the ai play against itself
(defun ai-v-ai ()
  (let ((board '(- - - - - - - - -)))
    (if (or (game-wonp board) (= (dash-count board) 0))
        (progn
        (format t "Game Complete! ~%")
        (print-board board))

        (progn
        (random-ai board)
        (print-board board)
        (format t "~%")
        (ai-v-ai)))))

;;This is the driver for the user to play against the AI as O
;;It runs recursively checking if the Game
;;is finished each time and if so stops the recursion.
(defun run-o()
  (let ((board '(- - - - - - - - -)))
    (if (or (game-wonp board) (= (dash-count board) 0))
        (progn
        (format t "Game Complete! ~%")
        (print-board board))

        (if (= (mod (dash-count board) 2) 0)
          (progn
          (get-move board)
          (print-board board)
          (format t "~%")
          (run-o))

          (progn
          (random-ai board)
          (print-board board)
          (format t "~%")
          (run-o))))))
          
;;This is the driver for the user to play against the AI as X
;;It runs recursively checking if the Game
;;is finished each time and if so stops the recursion.
(defun run-x()
  (let ((board '(- - - - - - - - -)))
    (if (or (game-wonp board) (= (dash-count board) 0))
        (progn
        (format t "Game Complete! ~%")
        (print-board board))

        (if (= (mod (dash-count board) 2) 0)
          (progn
          (random-ai board)
          (print-board board)
          (format t "~%")
          (run-x))

          (progn
          (get-move board)
          (print-board board)
          (format t "~%")
          (run-x))))))
